import m from "mithril";

export const Header = {
  view: () => {
    return m("div", m("h1", "Super Secret Project"));
  }
}