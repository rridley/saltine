import "bootstrap";
import m from "mithril";
import { Footer } from "./layout/Footer";
import { Header } from "./layout/Header";

import { GroupsView } from "./views/GroupListView";

import "./App.scss";

const layout = {
  view: (v: m.Vnode) => m("div.container", m(Header), v.children, m(Footer)),
} as m.Component;

const mountNode = document.querySelector("#app");

if (mountNode) {
  m.route(mountNode, "/", {
    "/": {
      render: () => m(layout, m(GroupsView)),
    },
  });
}
