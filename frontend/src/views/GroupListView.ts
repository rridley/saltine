import m from "mithril";
import { Groups } from "../models/Groups";

export const GroupsView = {
  oninit: () => {
    Groups.load();
  },
  view: () => {
    return m(
      "div",
      m("table.table", [
        m("thead", [m("tr", [m("th", "Name")])]),
        m(
          "tbody",
          Groups.data.map((group) => {
            return m("tr", [m("td", group.name)]);
          })
        ),
      ])
    );
  },
};
