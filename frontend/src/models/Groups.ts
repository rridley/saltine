import { API } from "./API";

export interface IGroup {
  id: number;
  name: string;
}

export const Groups = {
  data: [] as IGroup[],
  load: async () => {
    const results = await API.request<IGroup[]>({
      url: "http://localhost:5555/api/groups",
    });
      
    Groups.data = results;
  }
}