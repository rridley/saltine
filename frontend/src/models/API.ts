import m from "mithril";

export const API = {
  request: async <T> (options: m.RequestOptions<T> & { url: string }): Promise<T> => {
    // Get token details
    // TODO auth
    const auth = {
      name: "x-api-token",
      value: "test"
    }

    options.config = (xhr: any) => {
      xhr.setRequestHeader(auth.name, auth.value);
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    };

    const results = m.request(options).catch((error) => {
      if (error.code == 401) m.route.set("/login");
      throw error;
    });
    
    return results;
  },
  /*
  setToken: (value) => {
    localStorage.setItem("token", value);
  },
  flexToken: async () => {
    if (localStorage.getItem("token")) {
      // Allow token based auth
      return {
        name: "x-api-token",
        value: localStorage.getItem("token"),
      }
    } else if (localStorage.getItem("username")) {
      // Do oauth auth
      const results = await getTokenPopup({
        scopes: ["0c717c6e-90d5-4065-bd1c-9eac824f95ba/General"],
      });

      return {
        name: "Authorization",
        value: `Bearer ${results.accessToken}`,
      }
    }

    // Forward to login page if none are applicable
    m.route.set("/login");
    return;
  },
  tokenRemove: () => localStorage.removeItem("token"),
  */
};
