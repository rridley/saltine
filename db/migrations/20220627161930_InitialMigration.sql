-- migrate:up
create table groups (
  id int generated always as identity,
  name varchar not null,
  pillar jsonb
);

create table minions (
  minion_id varchar primary key not null,
  group_id int8,
  key_status varchar,
  grains jsonb,
  last_seen timestamptz default now()
);

create table users (
  id int generated always as identity,
  username varchar,
  password varchar
);

-- migrate:down
drop table minions;
drop table groups;
drop table users;
