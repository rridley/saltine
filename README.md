# Saltine

Saltine is an opinionated take on providing a clean interface to a SaltStack infrastructure.

**Note: This is under active development**

## Why
This tool is much less a "frontend" for SaltStack and more of a re-imagining of what SaltStack could be without having to re-invent the wheel. Rather than re-implementing all the fantastic work that SaltStack has done with it's system - I'm simply re-using it in a new way.

This allows SaltStack to stay fairly low level while providing a high level, easy to use, clean interface into it.