package salt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/r3labs/sse/v2"
)

type Salt struct {
	URL      string
	Username string
	Password string
	Auth     string

	saltCredentials *saltCredentials
}

var handlers []SaltEventHandler

func (s *Salt) Start() {
	// Create login token
	s.login()

	// Start the events processor
	go s.startEvents()
}

func (s *Salt) login() {
	body, err := json.Marshal(SaltLoginRequest{
		Username: s.Username,
		Password: s.Password,
		Auth:     s.Auth,
	})

	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/login", s.URL), bytes.NewBuffer(body))
	if err != nil {
		panic(err)
	}

	req.Header.Add("Content-Type", "application/json")

	client := http.Client{}

	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	// TODO Handle 401 response

	defer response.Body.Close()

	data, _ := ioutil.ReadAll(response.Body)

	log.Printf("Got auth response from Salt: %s\n", data)

	dd := &loginResponse{}

	err = json.Unmarshal(data, &dd)
	if err != nil {
		panic(err)
	}

	if len(dd.Return) != 1 {
		panic("No credentials returned")
	}

	s.saltCredentials = &dd.Return[0]
}

// Start the Salt events processor
func (s *Salt) startEvents() {
	if s.saltCredentials == nil {
		log.Printf("No login found, refreshing")
		s.login()
	} else if s.saltCredentials.Expire <= float64(time.Now().Unix()) {
		log.Printf("No login found, refreshing")
		s.login()
	}

	client := sse.NewClient(fmt.Sprintf("%s/events?token=%s", s.URL, s.saltCredentials.Token))

	log.Println("Subscribing")
	client.Subscribe("", func(msg *sse.Event) {
		// Unmarshal the basic event
		event := &SaltEvent{}
		err := json.Unmarshal(msg.Data, event)
		if err != nil {
			log.Printf("Failed to unmarshal event %+v", string(msg.Data))
			return
		}

		// Send to any matching handlers
		for _, handler := range handlers {
			if (handler.Matcher == "" || handler.Matcher == MatcherExact) && event.Tag == handler.Tag {
				handler.Handler(event)
			} else if handler.Matcher == MatcherStartsWith && strings.HasPrefix(event.Tag, handler.Tag) {
				handler.Handler(event)
			} else if handler.Matcher == MatcherMQTT && s.mqttMatcher(handler.Tag, event.Tag) {
				handler.Handler(event)
			}
		}
	})
}

// Subscribe to a Salt event tag
func (s *Salt) Subscribe(handler SaltEventHandler) {
	handlers = append(handlers, handler)
}

// Execute a Salt command
func (s *Salt) Execute(command *SaltRequest, output any) error {
	data, err := json.Marshal(command)
	if err != nil {
		return err
	}

	// Credential check
	if s.saltCredentials == nil {
		log.Printf("No login found, refreshing")
		s.login()
	} else if s.saltCredentials.Expire <= float64(time.Now().Unix()) {
		log.Printf("Expired token found, refreshing")
		s.login()
	}

	// Execute request
	log.Printf("Sending request: %s", data)

	req, err := http.NewRequest("POST", os.Getenv("SALT_URL"), bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("x-auth-token", s.saltCredentials.Token)

	client := http.Client{}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	defer res.Body.Close()

	response, _ := ioutil.ReadAll(res.Body)

	log.Printf("Got response: %s", response)

	// Unmarshal the response into the wrapper
	wrapper := &SaltReturnWrapper{}
	err = json.Unmarshal(response, wrapper)
	if err != nil {
		return err
	}

	// If an output object was provided attempt to unmarshal into the
	// output object
	if output != nil {
		log.Println("Attempting to unmarshal to output")
		err := json.Unmarshal(*wrapper.Return[0], output)
		if err != nil {
			return err
		}
	}

	return nil
}

func (s *Salt) mqttMatcher(filter string, topic string) bool {
	filterArray := strings.Split(filter, "/")

	length := len(filterArray)
	topicArray := strings.Split(topic, "/")

	for i := 0; i < length; i++ {
		left := filterArray[i]
		right := topicArray[i]

		if left == "#" {
			return len(topicArray) >= length-1
		}

		if left != "+" && left != right {
			return false
		}
	}

	return length == len(topicArray)
}
