package salt

import "encoding/json"

const (
	MatcherExact      = "EXACT"
	MatcherStartsWith = "STARTS_WITH"
	MatcherMQTT       = "MQTT"
)

type SaltLoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Auth     string `json:"eauth"`
}

type SaltRequest struct {
	Client     string                 `json:"client"`
	Target     string                 `json:"tgt"`
	TargetType string                 `json:"tgt_type,omitempty"`
	Function   string                 `json:"fun"`
	Arguments  map[string]interface{} `json:"kwarg,omitempty"`
}

type loginResponse struct {
	Return []saltCredentials `json:"return"`
}

type saltCredentials struct {
	Token  string   `json:"token"`
	Expire float64  `json:"expire"`
	Start  float64  `json:"start"`
	User   string   `json:"user"`
	Eauth  string   `json:"eauth"`
	Perms  []string `json:"perms"`
}

type SaltAsyncResponse struct {
	JobID   string   `json:"jid"`
	Minions []string `json:"minions"`
}

type SaltEvent struct {
	Tag string `json:"tag"`

	// With Salt this could literally be a billion different things so we need to
	// let the downstream handler's deal with Unmarshalling it
	Data *json.RawMessage `json:"data"`
}

type SaltEventHandler struct {
	Tag     string
	Matcher string
	Handler func(*SaltEvent)
}

type SaltReturnWrapper struct {
	Return []*json.RawMessage `json:"return"`
}
