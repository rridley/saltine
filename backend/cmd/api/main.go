package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"os"

	"codeberg.org/rridley/secretproject/pkg/salt"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
	"github.com/uptrace/bun/driver/pgdriver"
)

var client = salt.Salt{
	Username: os.Getenv("SALT_USERNAME"),
	Password: os.Getenv("SALT_PASSWORD"),
	Auth:     os.Getenv("SALT_EAUTH"),
	URL:      os.Getenv("SALT_URL"),
}

var db *bun.DB

func main() {
	// Setup Database
	dsn := os.Getenv("DATABASE_URL")
	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(dsn)))

	db = bun.NewDB(sqldb, pgdialect.New())

	// Setup SaltStack
	log.Println("Starting Salt engine")
	client.Start()

	client.Subscribe(salt.SaltEventHandler{
		Tag:     "salt/auth",
		Matcher: salt.MatcherExact,
		Handler: handleSaltAuthEvents,
	})

	client.Subscribe(salt.SaltEventHandler{
		Tag:     "salt/job/+/ret/+",
		Matcher: salt.MatcherMQTT,
		Handler: handleGrainEvents,
	})

	// Setup HTTP Server
	log.Println("Starting HTTP server")

	r := mux.NewRouter()

	r.Use(authenticationMiddleware)

	// Basic endpoints
	r.HandleFunc("/api/groups", groups).Methods("GET")
	r.HandleFunc("/api/minions", minions).Methods("GET")
	r.HandleFunc("/api/groups/{groupId}/minions", groupMinions).Methods("GET")
	r.HandleFunc("/api/minions/{minionId}", updateMinion).Methods("PUT")
	r.HandleFunc("/api/minions/{minionId}/refresh", refreshMinion).Methods("POST")

	// Salt endpoints
	r.HandleFunc("/api/salt/minions/{minionId}/test/ping", testPing).Methods("GET")
	r.HandleFunc("/api/salt/minions/{minionId}/service/restart", serviceRestart).Methods("POST")

	// TODO I think Gorilla can just handle this
	handler := cors.Default().Handler(r)

	s := &http.Server{
		// TODO user adjustable port
		Addr:    ":5555",
		Handler: handler,
	}
	s.ListenAndServe()
}

// Authentication middleware handler
func authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("X-API-Token")

		if token == "test" {
			// We found the token in our map
			log.Printf("Authenticated user %s\n", token)
			// Pass down the request to the next middleware (or final handler)
			next.ServeHTTP(w, r)
		} else {
			// Write an error and stop the handler chain
			http.Error(w, "Forbidden", http.StatusForbidden)
		}
	})
}

type ServiceRestartRequest struct {
	Name string `json:"name"`
}

func serviceRestart(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// Build request
	d := json.NewDecoder(r.Body)
	t := &ServiceRestartRequest{}

	err := d.Decode(&t)
	if err != nil {
		panic(err)
	}

	args := make(map[string]interface{})
	args["name"] = t.Name

	err = client.Execute(&salt.SaltRequest{
		Target:    vars["minionId"],
		Client:    "local",
		Function:  "service.restart",
		Arguments: args,
	}, nil)

	if err != nil {
		panic(err)
	}
}

// Run's a test.ping against a minion
func testPing(w http.ResponseWriter, r *http.Request) {
	output := make(map[string]bool)

	vars := mux.Vars(r)
	err := client.Execute(&salt.SaltRequest{
		Target:   vars["minionId"],
		Client:   "local",
		Function: "test.ping",
	}, &output)

	if err != nil {
		panic(err)
	}

	// Parse this
	data, _ := json.Marshal(output)

	w.Write(data)
}

// Return all groups
func groups(w http.ResponseWriter, r *http.Request) {
	var groups []Group

	err := db.NewSelect().Model(&groups).Scan(context.Background())
	if err != nil {
		panic(err)
	}

	// Turn to json
	output, err := json.Marshal(groups)
	if err != nil {
		panic(err)
	}

	w.Write(output)
}

// Will return a list of all minions that have been identified by the system
func minions(w http.ResponseWriter, r *http.Request) {
	var minions []Minion

	err := db.NewSelect().Model(&minions).Scan(context.Background())
	if err != nil {
		panic(err)
	}

	// Marshall
	output, err := json.Marshal(minions)
	if err != nil {
		panic(err)
	}

	w.Write(output)
}

// Will return a list of all minions assigned to a specific group
func groupMinions(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var minions []Minion

	err := db.NewSelect().Model(&minions).Where("group_id = ?", vars["groupId"]).Scan(context.Background())
	if err != nil {
		panic(err)
	}

	// Marshall
	output, err := json.Marshal(minions)
	if err != nil {
		panic(err)
	}

	w.Write(output)
}

// Update a minion
func updateMinion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// get the current minion
	var minion Minion

	d := json.NewDecoder(r.Body)

	err := d.Decode(&minion)
	if err != nil {
		panic(err)
	}

	// Ensure we only update fields that aren't coming from Salt
	_, err = db.NewUpdate().
		Model(&minion).
		Where("minion_id = ?", vars["minionId"]).
		Column("group_id").
		Exec(context.Background())
	if err != nil {
		panic(err)
	}
}

// Update a minion
func refreshMinion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	err := client.Execute(&salt.SaltRequest{
		Target:   vars["minionId"],
		Client:   "local",
		Function: "grains.items",
	}, nil)

	if err != nil {
		panic(err)
	}
}
