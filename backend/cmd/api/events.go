package main

import (
	"context"
	"encoding/json"
	"log"

	"codeberg.org/rridley/secretproject/pkg/salt"
)

func handleSaltAuthEvents(event *salt.SaltEvent) {
	log.Println("into handleSaltEvent")
	auth := &SaltAuthEvent{}
	err := json.Unmarshal(*event.Data, auth)
	if err != nil {
		panic(err)
	}

	minion := &Minion{
		MinionID:  auth.ID,
		KeyStatus: auth.Act,
	}

	_, err = db.NewInsert().
		Model(minion).
		On("CONFLICT (minion_id) DO UPDATE").
		Set("minion_id = EXCLUDED.minion_id").
		Set("key_status = EXCLUDED.key_status").
		Set("last_seen = now()").
		Exec(context.Background())
	if err != nil {
		panic(err)
	}
}

// Cache a minions grains anytime a grains.items is run so that we can
// leverage that data more easily.
func handleGrainEvents(event *salt.SaltEvent) {
	result := &SaltGrainEvent{}
	err := json.Unmarshal(*event.Data, result)

	// If we can't unmarshall it just ignore it, it's not super critical
	if err != nil {
		return
	}

	minion := &Minion{
		MinionID: result.MinionID,
		Grains:   result.Return,
	}

	if result.Function == "grains.items" {
		log.Println("Processing grains event")
		_, err = db.NewInsert().
			Model(minion).
			On("CONFLICT (minion_id) DO UPDATE").
			Set("minion_id = EXCLUDED.minion_id").
			Set("last_seen = now()").
			Set("grains = EXCLUDED.grains").
			Exec(context.Background())
		if err != nil {
			panic(err)
		}
	}
}
