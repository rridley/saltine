package main

import (
	"time"

	"github.com/uptrace/bun"
)

type Group struct {
	bun.BaseModel `bun:"table:groups,alias:u"`

	ID   int64  `bun:",pk,autoincrement" json:"id"`
	Name string `json:"name"`
}

type Minion struct {
	bun.BaseModel `bun:"table:minions,alias:u"`

	MinionID string    `bun:"minion_id,pk" json:"minionId"`
	GroupID  *int      `bun:"group_id,nullzero" json:"groupId"`
	LastSeen time.Time `bun:"last_seen,default:current_timestamp" json:"lastSeen"`

	KeyStatus string                  `bun:"key_status" json:"keyStatus"`
	Grains    *map[string]interface{} `bun:"grains,type:jsonb" json:"grains"`
}

type SaltAuthEvent struct {
	Stamp  string `json:"_stamp"`
	Act    string `json:"act"`
	ID     string `json:"id"`
	Pub    string `json:"pub"`
	Result bool   `json:"result"`
}

type SaltGrainEvent struct {
	Function string                  `json:"fun"`
	MinionID string                  `json:"id"`
	Return   *map[string]interface{} `json:"return"`
}
