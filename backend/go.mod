module codeberg.org/rridley/secretproject

go 1.18

require (
	github.com/gorilla/mux v1.8.0
	github.com/rs/cors v1.8.2
	github.com/uptrace/bun v1.1.5
	github.com/uptrace/bun/dialect/pgdialect v1.1.5
	github.com/uptrace/bun/driver/pgdriver v1.1.5
)

require (
	golang.org/x/net v0.0.0-20211112202133-69e39bad7dc2 // indirect
	gopkg.in/cenkalti/backoff.v1 v1.1.0 // indirect
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/r3labs/sse/v2 v2.8.0
	github.com/tmthrgd/go-hex v0.0.0-20190904060850-447a3041c3bc // indirect
	github.com/vmihailenco/msgpack/v5 v5.3.5 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/crypto v0.0.0-20220511200225-c6db032c6c88 // indirect
	golang.org/x/sys v0.0.0-20220627191245-f75cf1eec38b // indirect
	mellium.im/sasl v0.2.1 // indirect
)
